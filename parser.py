import os
import graph as gp
import os

#Discuss parser being a class and rename to IO
#Might make sense to generate the out files here as well in that case parser should be a class
def parseInputFile(file):

	#Necessary initializations
	f = open(file)
	Grafo = gp.graphConstructor()

	#Running Python with -O removes the printing
	#Read First Line to get N -- Number of Cities -- and L number of connections
	[N,L] = f.readline().rstrip('\n').split(" ")
	for i in range(int(N)):
		Grafo.addNode(i+1)
	#Add Edges
	for line in f:
		line = line.rstrip('\n')
		Grafo.addEdge(*line.split(" "))
	return Grafo

def parseClientFile(file):
	listofclients = []
	
	f = open(file)
	N = f.readline().rstrip('\n')
	for line in f:
		line = line.rstrip('\n')		
		client = line.split(" ")
		listofclients.append(client)
		
	return listofclients

def parseOutputFile(client,clientFile,time=None,cost=None,transports=None,path=None):

	outString = ''
	solFile = clientFile[:-3] + str('sol')
	outString+=str(client)+str(' ')

	if not os.path.isfile(solFile):
		open(solFile, 'a').close()

	if path[-1] != -1:
		ant = []
		for i in range(len(path)):
			if ant == []:
				ant = i
				outString+=str(path[i])+str(' ')
				continue
			else:
			    outString+=str(transports[ant]+' '+str(path[i])+' ')
			    ant = i
		outString+=str(time)+' '+str(cost)+'\n'
	else:
		outString+='-1'+'\n'

	with open(solFile, 'a') as f:
    		f.writelines(outString)

