import networkx as nx
import matplotlib.pyplot as plt

class graphConstructor:

	def __init__(self):
		self.Grafo = nx.MultiGraph()

	def addNode(self,node):
		self.Grafo.add_node(int(node))

	def addEdge(self,nodeFrom,nodeTo,edgeTransport,edgeTime,edgeCost,edgeTi,edgeTf,edgePeriod):
		#print nodeFrom,nodeTo,edgeTransport,edgeTime,edgeCost,edgeTi,edgeTf,edgePeriod
		self.Grafo.add_edge(int(nodeFrom), int(nodeTo), transport=edgeTransport, time=int(edgeTime), cost=int(edgeCost), Tf=int(edgeTf), Ti=int(edgeTi), P=int(edgePeriod))
		
	def editEdge(self,nodeFrom,nodeTo,edgeTransport,edgeTime,edgeCost,edgeTf,edgeTi,edgePeriod):
		""" Nos queremos passar uma aresta de autocarro para camiao, ou so queremos eliminar essa e fazer outra? """
		self.Grafo.add_edge(int(nodeFrom), int(nodeTo), transport=edgeTransport, time=int(edgeTime), cost=int(edgeCost), Tf=int(edgeTf), Ti=int(edgeTi), P=int(edgePeriod))

	def deleteEdge(self,nodeFrom,nodeTo,receiptKey):
		self.Grafo.remove_edge(int(nodeFrom),int(nodeTo),key=receiptKey)

	def deleteEdgesWith(self,item,limit):
		edges = self.getAllEdges(in_data=True)
		for edge in edges:
			if item == 'transport' and edge[3][item] == limit:
				self.deleteEdge(edge[0],edge[1],edge[2])
			elif (item == 'cost' and edge[3][item] > limit) or (item == 'time' and edge[3][item] > limit):
				self.deleteEdge(edge[0],edge[1],edge[2])

	def printGraph(self,item):
		"""Ainda nao funciona com duas arestas entre os mesmos nos, mas hei-de perceber porque. De qualquer maneira, e so para ser bonito, mas pode ser alterado"""
		pos = nx.spring_layout(self.Grafo)
		nx.draw(self.Grafo, pos)
		edge_labels=dict([((u,v,),d[item])
		for u,v,d in self.Grafo.edges(data=True)])
		nx.draw_networkx_edge_labels(self.Grafo, pos, edge_labels=edge_labels, label_pos=0.3, font_size=7)
		plt.show()
		#plt.savefig('foo.png')

	def numberEdges(self,nodeFrom=None,nodeTo=None):
		if nodeFrom == None:
			print(self.Grafo.number_of_edges())
		else:
			print(self.Grafo.number_of_edges(nodeFrom,nodeTo))

	def getAllEdges(self,node=None,in_data=False,in_keys=True):
		return self.Grafo.edges(node,data=in_data,keys=in_keys)

	def getEdges(self,nodeFrom=None,nodeTo=None,key=None,in_data=False,in_keys=True):
		outList = []
		for i,val in enumerate(self.getAllEdges(nodeFrom, in_data)): 
			#print val[0],val[1],val[2]
			if key == None and val[1] == nodeTo:
				outList.append(val)
			elif val[2] == key and val[1] == nodeTo:
				outList.append(val)

		return outList
		
	def getNodes(self):
		return self.Grafo.nodes()

	def getNodeNeighbors(self,node):
		return nx.all_neighbors(self.Grafo,node)





	