import os
import graph as gp
import math as math
import operator as op
import parser
import sys
import nonheuristic as nh
import heuristic as h
import datetime as dt


def getCost(graph,nodeFrom,nodeTo,optim,timeReceived=0,heuristicMode=0):
	edges = graph.getEdges(nodeFrom,nodeTo,in_data=True)

	minEdgeCost = float("inf")
	minTimeCost = float("inf")
	timeAfterEdge = float("inf")

	timeToNextTransport = 0

	minEdgeKey = None
	minEdgeTransport = ''

	for edge in edges:	
		totalEdgeTime, edgeCost = getEdgeTimeCost(edge,receiptTime=timeReceived,heuristic=heuristicMode)
		if optim == 'time' and ((totalEdgeTime + timeReceived) < timeAfterEdge):
			minEdgeCost = edgeCost
			minEdgeTransport = edge[3]['transport']
			minEdgeKey = edge[2]
			timeAfterEdge = timeReceived+totalEdgeTime
		elif optim == 'cost' and edge[3]['cost'] < minEdgeCost:
			minEdgeCost = edgeCost
			minEdgeTransport = edge[3]['transport']
			minEdgeKey = edge[2]
			timeAfterEdge = timeReceived+totalEdgeTime
	return minEdgeKey, minEdgeCost, minEdgeTransport, timeAfterEdge


def getEdgeTimeCost(edge,receiptTime=0,heuristic=0):
	""""""

	transportPeriod = edge[3]['P']
	transportStart = edge[3]['Ti']
	transportEnd = edge[3]['Tf'] 
	timeUntilEOD = 0
	days = 0
	decTime = receiptTime
	timeToNextTransport = 0
	if heuristic == 0:
		while decTime > 1440:
			decTime -= 1440
		
		dayTime = decTime
		
		t = transportStart
		while t < dayTime:
			t+=transportPeriod

		if t > 1440 or t > transportEnd:
			timeToNextTransport+=(1440-dayTime)+transportStart
			
		else:
			timeToNextTransport = t - dayTime

	totalTime = timeToNextTransport+edge[3]['time']
	Cost = edge[3]['cost']

	return totalTime, Cost

def findBestCost(graph,path,optim,receiptTime=0):
	nodes = []
	value = []
	transports=[]
	ant = []
	time = receiptTime
	acumCost = 0

	for nodeTo in path:
		if ant == []:
			ant = nodeTo
			continue
		else:
			minEdgeKey, minEdgeCost, minEdgeTransport, timeAfterEdge = getCost(graph,ant,nodeTo,optim,time)
			
			time = timeAfterEdge
			acumCost += minEdgeCost
			transports.append(minEdgeTransport)
			ant = nodeTo

	return acumCost, transports, time



def calcOut(mapFile,cliFile,algorithm='a_star'):
	clients = parser.parseClientFile(cliFile)
	for client in clients:
		print(int(client[0]),' Working.')
		yensFlag = 0
		Grafo = parser.parseInputFile(mapFile)
		maxOptimValue = float("inf")

		for i in range(int(client[5])):
			if client[6+2*i] == 'A1':
				Grafo.deleteEdgesWith('transport',client[7+2*i])

			if client[6+2*i] == 'A2':
				Grafo.deleteEdgesWith('time',int(client[7+2*i]))

			if client[6+2*i] == 'A3':
				Grafo.deleteEdgesWith('cost',int(client[7+2*i]))

			if (client[6+2*i] == 'B1' or client[6+2*i] == 'B2') and (algorithm != 'yens'):
				#print "Restriction",client[6+2*i],"cannot be accomplished with speciefied or default algorithm. Will use Yen's algorithm."
				yensFlag = 0
				maxOptimValue = int(client[7+2*i])
			elif (client[6+2*i] == 'B1' or client[6+2*i] == 'B2') and (algorithm == 'yens'):
				maxOptimValue = int(client[7+2*i])

		if yensFlag == 0 and algorithm != 'yens':
			if algorithm=='dfs':

				path = nh.dfs(Grafo,int(client[1]),int(client[2]))
				if path[-1] != int(client[2]):
					path = [-1]

				if client[4] == 'tempo':
					optim = 'time'
				elif client[4] == 'custo':
					optim = 'cost'

				cost, transports, time = findBestCost(Grafo,path,optim,int(client[3]))

				parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
				print(int(client[0]),' Done DFS.')

			elif algorithm=='bfs':
				path = nh.bfs(Grafo,int(client[1]),int(client[2]))
				if path[-1] != int(client[2]):
					path = [-1]

				if client[4] == 'tempo':
					optim = 'time'
				elif client[4] == 'custo':
					optim = 'cost'

				cost, transports, time = findBestCost(Grafo,path,optim,int(client[3]))
				parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
				print(int(client[0]),' Done BFS.')

			elif algorithm=='bibfs':
				path = nh.bidirectionalBfs(Grafo,int(client[1]),int(client[2]))
				if path[-1] != int(client[2]):
					path = [-1]

				if client[4] == 'tempo':
					optim = 'time'
				elif client[4] == 'custo':
					optim = 'cost'

				cost, transports, time = findBestCost(Grafo,path,optim,int(client[3]))
				parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
				print(int(client[0]),' Done BiBFS.')		
			elif algorithm=='uniformCost':
				if client[4] == 'tempo':
					optim = 'time'
				elif client[4] == 'custo':
					optim = 'cost'

				path, cost, time, transports = nh.uniformCost(Grafo,int(client[1]),int(client[2]),optim,int(client[3]))
				
				if path[-1] != int(client[2]):
					path = [-1]

				parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
				print(int(client[0]),' Done UniformCost.')

			elif algorithm=='a_star':
				if client[4] == 'tempo':
					optim = 'time'
				elif client[4] == 'custo':
					optim = 'cost'
				
				path, cost, time, transports = h.a_star(Grafo,int(client[1]),int(client[2]),optim,timeStart = int(client[3]), maxOptim = maxOptimValue)
				
				if path[-1] != int(client[2]):
					path = [-1]

				parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
				print(int(client[0]),' Done A*.')
		if yensFlag == 1 or algorithm=='yens':
			if client[4] == 'tempo':
				optim = 'time'
			elif client[4] == 'custo':
				optim = 'cost'

			path, transports, cost, time = h.Yens(Grafo,int(client[1]),int(client[2]),optim,timestart = int(client[3]), maxOptim = maxOptimValue)

			if path[-1] != int(client[2]):
				path = [-1]

			parser.parseOutputFile(int(client[0]),cliFile,time,cost,transports,path)
			print(int(client[0]),' Done Yens.')
