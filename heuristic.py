import os
import graph as gp
import math as math
import operator as op
import pq
import util as ut
import copy
import time

def a_star(graph,start,goal, optim = 'cost',timeStart = 0,  maxOptim = float("inf")):
	"""FALTA ADICIONARES CUSTO DE TEMPO AO LONGO DA RUN, NAO TENS AINDA"""
	heuristicVector = heuristic(graph,goal)
	#Frontier now is a priority queue
	Frontier = pq.pq()
	
	#Add start node with 0 priority
	Frontier.put(start,0)

	#Tipical A* trackers
	cameFrom 		= {}
	costSoFar 		= {}
	transportSoFar 	= {}
	timeSoFar		= {}

	#initializations
	cameFrom[start] = None
	costSoFar[start] = 0
	timeSoFar[start] = timeStart

	while not Frontier.empty():
		current = Frontier.get()

		if current == goal:
			if ((optim == 'cost' and costSoFar[goal]<maxOptim) or (optim == 'time' and timeSoFar[goal] < maxOptim)):
				j = goal
				finalPath = []
				trasportPath = []
				finalPath.append(j)
				trasportPath.append(transportSoFar[j])
				while j != start:
					j = cameFrom[j]
					finalPath.append(j)
					if j!=start:trasportPath.append(transportSoFar[j])
				finalPath = list(reversed(finalPath))
				return finalPath, costSoFar[goal], timeSoFar[goal] - timeStart, trasportPath
			return [-1],0,0,[]
		for nextNode in graph.getNodeNeighbors(current):
			#Iterate all edges between currentNode
			key,newCost,trasport,timeAfter = ut.getCost(graph,current,nextNode,optim = optim,timeReceived = timeSoFar[current])
			newCost += costSoFar[current] 
			if nextNode not in costSoFar or (newCost < costSoFar[nextNode] and optim == 'cost' and costSoFar[nextNode]) or (timeAfter < timeSoFar[nextNode] and optim == 'time'):
				costSoFar[nextNode] = newCost
				transportSoFar[nextNode] = trasport
				timeSoFar[nextNode] = timeAfter
				if optim == 'cost':
					priority = newCost + 0#+ Heuristic() Insert Heuristic here
				elif optim == 'time':
					priority = timeAfter + heuristicVector[nextNode]
				Frontier.put(nextNode,priority)
				cameFrom[nextNode] = current
	finalPath = [-1]
	costSoFar = [-1]
	timeSoFar = [-1]
	trasportPath = [-1]
	return finalPath, costSoFar, timeSoFar, trasportPath

def a_star_edge(graph,start,goal, optim = 'cost',timeStart = 0, heuristicVector=None):
	"""Modification of A* to work with edge iteration and not nodes"""
	#Frontier now is a priority queue
	Frontier = pq.pq()
	
	#Add start Edges with given priority	
	Frontier.put([start,start,0],0)

	#Tipical A* trackers
	cameFrom 		= {}
	costSoFar 		= {}
	timeSoFar 		= {}
	transportSoFar 	= {}
	final = []

	#initializations
	cameFrom[tuple([start,start,0])] = None
	costSoFar[tuple([start,start,0])] = 0
	timeSoFar[tuple([start,start,0])] = timeStart

	#While nodes exist on the frontier
	while not Frontier.empty():
		current = Frontier.get()
		
		#Test if solution is found
		if current[1] == goal:

			#Compute solution for return
			final = current
			finalPath = []
			finalVector = []
			a = final
			finalPath.append(a)
			finalVector.append(a[1])
			while(a!=[start,start,0]):
				a = cameFrom[a]
				finalPath.append(a)
				finalVector.append(a[1])
			return finalPath[0:-1], finalVector, costSoFar[final], timeSoFar[final] - timeStart

		#Iterate over Neighbors nodes
		for nextNode in graph.getNodeNeighbors(current[1]):
			
			#Iterate over all edges to that node
			for edge in graph.getEdges(current[1],nextNode,in_data = True, in_keys = True):
				
				#get that edge cost/time
				timeAfter, newCost = ut.getEdgeTimeCost(edge,receiptTime = timeSoFar[tuple(current)], heuristic = 0)
				newCost += costSoFar[tuple(current)]
				timeAfter += timeSoFar[tuple(current)]
				
				#if new value is better than update varaibles of each edge
				if edge[0:3] not in costSoFar or (newCost < costSoFar[edge[0:3]] and optim == 'cost') or (timeAfter < timeSoFar[edge[0:3]] and optim == 'time'):
					
					#update edge in forward direction
					costSoFar[edge[0:3]] = newCost
					costSoFar[edge[1],edge[0],edge[2]] = newCost
					
					#update edge in backward direction
					timeSoFar[edge[0:3]] = timeAfter
					timeSoFar[edge[1],edge[0],edge[2]] = timeAfter
					
					#Compute values for the edge given the heuristic
					if optim == 'cost':
						priority = newCost  + heuristicVector[nextNode]
					elif optim == 'time':
						priority = timeAfter + heuristicVector[nextNode]

					#update Frontier
					Frontier.put(edge[0:3],priority)
					cameFrom[edge[0:3]] = current
	
	#No solution Found (goal node was never on frontier and therefore not connected to the start)
	return [],[], float("inf"),float("inf")

def heuristic(graph,goal,optim = 'time'):
	"""Heuristic function is based on A* wihtout heuristic, what can be called a Dijkstra Algorithm."""
	#Init frontier
	Frontier = pq.pq()
	Frontier.put(goal,0)

	#Typical A* variables
	cameFrom 		= {}
	costSoFar 		= {}
	timeSoFar		= {}
	transportSoFar 	= {}

	#init values
	nodes = graph.getNodes()
	for i in range(len(nodes)):
		timeSoFar[i+1] = float("inf")
		costSoFar[i+1] = float("inf")
	cameFrom[goal] = None
	costSoFar[goal] = 0
	timeSoFar[goal] = 0

	#Iterate until frontier is empty
	while not Frontier.empty():
		current = Frontier.get()

		#get the neighbours of current node 
		for nextNode in graph.getNodeNeighbors(current):

			#Find least cost/time edge
			key,newCost,trasport,timeAfter = ut.getCost(graph,current,nextNode,optim = optim,timeReceived = timeSoFar[current],heuristicMode=1)
			newCost += costSoFar[current] 
			
			#If eedge is better than previous update node values 
			if nextNode not in costSoFar or (newCost < costSoFar[nextNode] and optim == 'cost') or (timeAfter < timeSoFar[nextNode] and optim == 'time'):
				costSoFar[nextNode] = newCost
				transportSoFar[nextNode] = trasport
				timeSoFar[nextNode] = timeAfter
				if optim == 'cost':
					priority = newCost
				elif optim == 'time':
					priority = timeAfter
				Frontier.put(nextNode,priority)
				cameFrom[nextNode] = current

	#Return should be a vector containing a relaxated information of cost/time from any node to goal 
	if optim == 'time':
		return timeSoFar
	if optim == 'cost':
		return costSoFar


def Yens(graph,start,goal,optim='cost',timestart = 0, maxOptim = float("inf")):
	
	#Typical Yen's algorithm variables
	A = []
	A1 = []
	B1 = []
	B = []
	Done = False
	best = float("inf")

	#Heuristic values calculated by relaxation of the problem
	heuristicVector = heuristic(graph,goal, optim)

	#Make first run of the algorithm to get best solution, store the result in A1
	A1.append(a_star_edge(graph,start,goal,optim,timestart, heuristicVector))

	#Store Graph in A
	A.append(graph)

	#Iterate all solutions by the best order until one "fits"
	while Done != True:

		#Test Solution
		if optim == 'cost' and A1[-1][2] < maxOptim:
			Done = True
			break
		if optim == 'time' and A1[-1][3] < maxOptim:
			Done = True
			break

		#Solution not valid and therefore compute next best solution
		for edge in A1[-1][0]:
			gp = copy.deepcopy(A[-1])

			#For each edge in the solution path delete
			gp.deleteEdge(edge[0],edge[1],edge[2])

			#Store the new best solution for the new graph and store it in B1 and B
			#Only store the solution if it is not present
			new_score = a_star_edge(gp,start,goal,optim,timestart,heuristicVector)
			if new_score not in B1:
				B1.append(new_score) 
				B.append(gp)
		
		bestIndex = -1
		best = float("inf")
		
		#Find best solution inside the B vector that contains all possible new best
		for i in range(len(B1)):
			if B1[i][2]<best and optim == 'cost':
				best = B1[i][2]
				bestIndex = i
			if B1[i][3]<best and optim == 'time':
				best = B1[i][3]
				bestIndex = i
		#No solution found if best is inf
		if(best == float("inf")):
			return [-1],[],0,0
			break
		
		#Append new best to A and remove it from B
		A.append(B[bestIndex])
		A1.append(B1[bestIndex])
		B.pop(bestIndex)
		B1.pop(bestIndex)

	#Solution Found! return it
	finalPath = list(reversed(A1[-1][1]))
	finalEdge = list(reversed(A1[-1][0]))
	finalTransport = []
	#Compute transports from the edges solution
	for edge in finalEdge:
		a = A[-1].getEdges(edge[0],edge[1],edge[2], in_data=True)
		finalTransport.append(a[0][3]['transport'])

	return finalPath, finalTransport, A1[-1][2], A1[-1][3]