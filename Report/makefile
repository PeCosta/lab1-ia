#############################################################
#  pdfLaTeX  Makefile  v1.5                                 #
#                                                           #
#  Made to work with TUG distribution of TeXLive 2012       #
#  http://www.tug.org/texlive/acquire-netinstall.html       #
#  and Ghostscript 9.05 for compressing                     #
#                                                           #
#  Tested successfully in windows with MikTeX 2.9           #
#  Requires Make for Windows                                #
#  Note: Windows is not case sensitive                      #
#  _______________________________________________________  #
#                                                           #
#  Author: Bruno Santos                                     #
#  _______________________________________________________  #
#                                                           #
#  Readme:                                                  #
#     Change $FILE to the name of the main project file     #
#     without extension.                                    #
#                                                           #
#     latexmk deals with dependencies alone and ensures     #
#     cross-references are dealt with.                      #
#                                                           #
#                                                           #
#     compile:                  make (s)ilent / (v)erbose   #
#                               make (c)ontinuous           #
#                                                           #
#     clean:                    make clean                  #
#     clean all:                make cleanall               #
#                                                           #
#############################################################



#############################################################
# Compilation

# -pdf: pdflatex; -f: continue after error
FLAGS = -pdf -f -auxdir=Aux -outdir=Aux

# Leave extension .tex out !!!
FILE = main
AUX = Aux/

# silent (default)
s:
	@ latexmk $(FLAGS) -silent $(FILE)

# verbose
v:
	@ latexmk $(FLAGS) $(FILE)

# preview continuously
c:
	@ latexmk $(FLAGS) -silent -pvc $(FILE)

#
run:
	okular $(AUX)$(FILE).pdf

#############################################################
# Maintenance

# compress with ghostscript
gs:
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(AUX)$(FILE)-compressed.pdf $(AUX)$(FILE).pdf


# leaves pdf
clean:
	@ rm -f $(AUX)$(FILE).log $(AUX)$(FILE).aux $(AUX)$(FILE).toc $(AUX)$(FILE).blg $(AUX)$(FILE).bbl $(AUX)$(FILE).out $(AUX)$(FILE).fls $(AUX)$(FILE).fdb_latexmk $(AUX)$(FILE).glo $(AUX)$(FILE).lof $(AUX)$(FILE).lot $(AUX)$(FILE).nlg $(AUX)$(FILE).nlo $(AUX)$(FILE).nls

# leaves only the source
cleanall:
	@ rm -f $(AUX)$(FILE).log $(AUX)$(FILE).aux $(AUX)$(FILE).toc $(AUX)$(FILE).blg $(AUX)$(FILE).bbl $(AUX)$(FILE).out $(AUX)$(FILE).fls $(AUX)$(FILE).fdb_latexmk $(AUX)$(FILE).glo $(AUX)$(FILE).lof $(AUX)$(FILE).lot $(AUX)$(FILE).nlg $(AUX)$(FILE).nlo $(AUX)$(FILE).nls $(AUX)$(FILE).pdf $(AUX)$(FILE)-compressed.pdf
	@ rm -f $(AUX)/*
