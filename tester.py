import glob
import os

os.chdir("traveltests/")
for file in glob.glob("*.map"):
    fileCommon = str(file)[:-3]
    command = "python3.4 ../istravel.py "+fileCommon+"map "+fileCommon+"cli"
    print(command)
    os.system(command)

print('Test COMPLETE!')
