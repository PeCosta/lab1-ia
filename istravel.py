import os
import graph as gp
import parser
import argparse
import util as ut 
from heuristic import heuristic
from heuristic import Yens

def main():
	parser = argparse.ArgumentParser(usage=__doc__)
	parser.add_argument("map", type=str, default='input1.map', help="Map file.")
	parser.add_argument("cli", type=str, default='input2.cli', help="Clients file.")
	parser.add_argument("-alg", type=str, default='a_star', help="Search algorithm preference.")
	args = parser.parse_args()

	if os.path.isfile(args.cli[:-3]+str('sol')):
		os.remove(args.cli[:-3]+str('sol'))

	ut.calcOut(args.map,args.cli,args.alg)

if __name__ == '__main__':
	main()