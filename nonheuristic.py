import os
import graph as gp
import math as math
import operator as op
import util as ut
import parser
import sys as s
import datetime as dt
import pq

def bfs(graph, start, goal):
	
	queue =[]
	visited = []
	queue.append([start])
	path = []
	while queue:
		path = queue.pop(0)
		
		node = path[-1]

		if node == goal:
			break
		
		for val in graph.getNodeNeighbors(node):
			if val not in visited:
				visited.append(val)
				new_path = list(path)
				new_path.append(val)
				queue.append(new_path)
	return path


def bidirectionalBfs(graph, start, goal):
	""""""

	#Initialization
	queue 	= [],[]
	visited = [],[]
	path 	= [],[]
	finalpath = []

	#Starting points for bidirectional search
	queue[0].append([start])
	queue[1].append([goal])

	#Initialize BFS matrix
	pathInwards = [None]*(len(graph.getNodes())+1)
	pathBackwards = [None]*(len(graph.getNodes())+1)

	#while there are nodes to explore
	while queue[0] and queue[1]:

		#pop next path to explore for both foward and backward search
		path = queue[0].pop(0),queue[1].pop(0);
		node = path[0][-1],path[1][-1]

		#Check if we have a solution
		for e in range(1,len(graph.getNodes())+1):

			#If a node is present in inward and backward path simultaniously the we have a solution
			if pathInwards[e] != None and pathBackwards[e] != None:
				j=i=e
				finalpath.append(j)
				while j != start:
					j = pathInwards[j]
					finalpath.append(j)

				#revert for presentation purpose
				finalpath = list(reversed(finalpath))
				while i != goal:
					i = pathBackwards[i]
					finalpath.append(i)
				return finalpath

		#Explore neighbours
		for val in graph.getNodeNeighbors(node[0]):
			if val not in visited[0]:
				pathInwards[val] = node[0]
				visited[0].append(val)
				new_path = list(path[0])
				new_path.append(val)
				queue[0].append(new_path)

		#Explore neighbours
		for val in graph.getNodeNeighbors(node[1]):
			if val not in visited[1]:
				pathBackwards[val] = node[1]
				visited[1].append(val)
				new_path = list(path[1])
				new_path.append(val)
				queue[1].append(new_path)
	return [-1]


def dfs(graph,start,goal,visitedQueue=None,path=None,transport=None):
	# Checks for initializations
	if visitedQueue == None:
		visitedQueue = []
	if path == None:
		path = []

	# Append to path and to visited queues the current node
	path.append(start)
	visitedQueue.append(start)

	# Recursive loop for depth search of nodes
	for val in graph.getNodeNeighbors(start):

		# Stops at goal
		if start == goal:
			return path

		# Keeps searching not visited nodes
		if val not in visitedQueue:
			path = dfs(graph,val,goal,visitedQueue,path)
			# If last element in path is not the objective, then go backwords in path
			if path[-1] != goal:
				del path[-1]

	return path


def uniformCost(graph,start,goal,optim='cost',timeStart=0):
	#Frontier now is a priority queue
	Frontier = pq.pq()
	
	#Add start node with 0 priority
	Frontier.put(start,0)

	#Tipical A* trackers
	cameFrom 		= {}
	costSoFar 		= {}
	transportSoFar 	= {}
	timeSoFar		= {}

	#initializations
	cameFrom[start] = None
	costSoFar[start] = 0
	timeSoFar[start] = timeStart

	while not Frontier.empty():
		current = Frontier.get()

		if current == goal:
			j = goal
			finalPath = []
			trasportPath = []
			finalPath.append(j)
			trasportPath.append(transportSoFar[j])
			while j != start:
				j = cameFrom[j]
				finalPath.append(j)
				if j!=start:trasportPath.append(transportSoFar[j])
			finalPath = list(reversed(finalPath))
			return finalPath, costSoFar[goal], timeSoFar[goal], trasportPath

		for nextNode in graph.getNodeNeighbors(current):
			#Iterate all edges between currentNode
			key,newCost,trasport,timeAfter = ut.getCost(graph,current,nextNode,optim = optim,timeReceived = timeSoFar[current])
			newCost += costSoFar[current] 
			#newTime += timeSoFar[current]--No need i think
			if nextNode not in costSoFar or (newCost < costSoFar[nextNode] and optim == 'cost') or (timeAfter < timeSoFar[nextNode] and optim == 'time'):
				costSoFar[nextNode] = newCost
				transportSoFar[nextNode] = trasport
				timeSoFar[nextNode] = timeAfter
				if optim == 'cost':
					priority = newCost
				elif optim == 'time':
					priority = timeAfter
				Frontier.put(nextNode,priority)
				cameFrom[nextNode] = current
	finalPath = [-1]
	costSoFar = [-1]
	timeSoFar = [-1]
	trasportPath = [-1]
	return finalPath, costSoFar, timeSoFar, trasportPath